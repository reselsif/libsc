#ifndef LIBSC_TREE_NODE_H_INCLUDED
#define LIBSC_TREE_NODE_H_INCLUDED



/*
 * LIBRARIES
 */
#include <stdlib.h>
#include "../list/list.h"





/*
 * PREDECLARATIONS
 */
// node of binarie tree, use void* values
typedef struct tree tree;
struct tree_node {
        // ATTRIBUTES
                void* value;
		list* sons; // list of sons
                struct tree_node* father; // father node
                tree* tree; // tree where node is
};
typedef struct tree_node tree_node;
typedef struct tree_node tree_iterator; // simple shortcut for librarie user






/*
 * PROTOTYPES
 */
// INITIALIZING
        int tree_node_init(tree_node*, void*, tree_node*, tree*);// initialize the given node
        tree_node* tree_node_malloc(void*, tree_node*, tree*); // creat, initialize and return a new node
        void* tree_node_free(tree_node*); // free the node and return value





#endif
