#include "tree.h"




/****************************************************************
 * INITIALIZING
 ****************************************************************/


/*
 * INIT
 */
// initialize the binarie tree
int tree_init(tree* t, void* rootValue) {
        if(t != NULL) {
                // ATTRIBUTES
                        t->root = tree_node_malloc(rootValue, NULL, t);
                        t->nodeCnt = 1; 
                        return 1;
        }
        return 0;
}



/*
 * BINARIE TREE MALLOC
 */
// Creat, initialize and return a binarie tree
tree* tree_malloc(void* value) {
        tree* bt = malloc(sizeof(tree));
        tree_init(bt, value);
        return bt;
}



/*
 * FREE
 */
// call a free on all the nodes, and desinitialize the tree.
// CAREFUL: values are not free !
void tree_free(tree* bt) {
        // free on all the nodes with postfix movement
        tree_postfix(bt->root, &tree_free_fonctor, NULL, NULL);
        // free on tree
        free(bt);
}


/*
 * FREE FONCTOR
 */
// Use by tree_free for free all the nodes
void tree_free_fonctor(tree_fonctor f) {
        tree_node_free(f.node);
}






/****************************************************************
 * PREDICATS
 ****************************************************************/


/*
 * HAVE SON
 */
// return 1 if node have a son or more, 0 else
int tree_haveSon(tree_node* node) {
          return (node != NULL && !list_isEmpty(node->sons));
}




/*
 * HAVE FATHER
 */
// return 1 if node have a father, 0 if not
int tree_haveFather(tree_node* it) {
          return (it != NULL && it->father != NULL);
}



/*
 * TARGET NODE
 */
// return 1 if iterator targets a node
int tree_targetNode(tree_node* it) {
        return (it != NULL);
}





/****************************************************************
 * ACCESSORS
 ****************************************************************/


/*
 * GET VALUE
 */
// return target node value
void* tree_getValue(tree_node* it) {
        if(it != NULL)
                return it->value;
        return NULL;
}



/*
 * SET VALUE
 */
// modify target node value and return the ancient value
void* tree_setValue(tree_node* it, void* value) {
        void* ancientValue = NULL;
        if(it != NULL) {
                ancientValue = it->value;
                it->value = value;
        }
        return ancientValue;
}



/*
 * GET ITERATOR
 */
// return iterator on root
tree_node* tree_getIterator(tree* bt) {
        return bt->root;
}



/*
 * GET SON
 */
// return son i of the node, or NULL
tree_node* tree_getSon(tree_node* node, int i) {
	return (list_getItem(node->sons, i))->value;
}





/****************************************************************
 * ALTERATIONS
 ****************************************************************/


/*
 * CREAT ROOT
 */
// creat root if no root in given tree
tree_node* tree_creatRoot(tree* bt, void* value) {
        if(bt->root != NULL) return bt->root;
        tree_node* it = tree_node_malloc(value, NULL, bt); // create node
        // initialize it
        if(it != NULL) {
		if(bt != NULL) {
                	bt->root = it;
			bt->nodeCnt = 1;
		}
        }
        return it;
}



/*
 * ADD SON
 */
// if possible, add a son to targeted node
tree_node* tree_addSon(tree_node* it, void* value) {
	tree_node* sonAdded = NULL;
        if(it != NULL) { // if tree is empty, nothing to do
		sonAdded = tree_node_malloc(value, it, it->tree);
		list_addItem(it->sons, sonAdded);
	}
	return sonAdded;
}







/****************************************************************
 * MOVEMENTS
 ****************************************************************/
// 2 patterns of movements : prefix and postfix
// fonctor is a void function, use void arguments (given in moves function at 3 arguments),
// and a return value (int type)
//      void (*fonctor)(tree_node*, void*, int*, int)


/*
 * POSTFIX
 */
// API for tree_postfix_rec function
void tree_postfix(tree_node* begin, void (*fonctor)(tree_fonctor), 
		void* argv, void* returnValue) {
	int termine = 0;
        tree_postfix_rec(begin, fonctor, argv, returnValue, &termine, 0);
}


/*
 * POSTFIX REC
 */
// apply fonctor on each node of given tree, in postfix order
void tree_postfix_rec(tree_node* it, void (*fonctor)(tree_fonctor), 
		void* argv, void* returnValue, int* termine, int depth) {
	list_iterator son = it->sons->first;
	// foreach son
        for(; list_targetItem(son) && !(*termine); son = son->next) {
		tree_postfix_rec(son->value, fonctor, argv, returnValue, termine, depth+1);
	}
	if(!(*termine)) {
		tree_fonctor f = {it, argv, returnValue, termine, depth};
		fonctor(f);
	}
}




/*
 * PREFIX
 */
// API for tree_prefix_rec function
void tree_prefix(tree_node* begin, void (*fonctor)(tree_fonctor), 
		void* argv, void* returnValue) {
	int termine = 0;
        tree_prefix_rec(begin, fonctor, argv, returnValue, &termine, 0);
}


/*
 * PREFIX REC
 */
// apply fonctor on each node of given tree, in prefix order
void tree_prefix_rec(tree_node* it, void (*fonctor)(tree_fonctor), 
		void* argv, void* returnValue, int* termine, int depth) {
	list_iterator son = it->sons->first;
	if(!(*termine)) {
		tree_fonctor f = {it, argv, returnValue, termine, depth};
		fonctor(f);
	}
	// foreach son
        for(; list_targetItem(son) && !(*termine); son = son->next) {
		tree_prefix_rec(son->value, fonctor, argv, returnValue, termine, depth+1);
	}
}





