#ifndef LIBSC_TREE_INCLUDED
#define LIBSC_TREE_INCLUDED

/*
 * LIBRARIES
 */
#include "tree_node.h"



/*
 * STRUCTURES
 */
// binarie tree, modes is tree type
struct tree {
        // ATTRIBUTS
        tree_node *root; // root of binarie tree
        int nodeCnt; // number of nodes
};

// fonctor's arguments
typedef struct {
	tree_node* node; // node
	void* argv; // argument send for fonctor
	void* ret; // value return 
	int* termine; // if true, moves in tree is interupt
	int depth; // depth of node
} tree_fonctor;



/*
 * PROTOTYPES
 */
// INITIALIZING
        int tree_init(tree*, void*); // initialize the binarie tree
        tree* tree_malloc(void*); // Creat, initialize and return a binarie tree
        void tree_free(tree*); // call a free on all the nodes, and desinitialize the tree.
        void tree_free_fonctor(tree_fonctor); // Use by tree_free for free all the nodes

// PREDICATS
        int tree_haveSon(tree_node*); // return 1 if node have a son or more, 0 else
        int tree_haveFather(tree_node*); // return 1 if node have a father, 0 if not
        int tree_targetNode(tree_node* it); // return 1 if iterator targets a node

// ACCESSORS
        void* tree_getValue(tree_node*); // return target node value
        void* tree_setValue(tree_node*, void*); // modify target node value and return the ancient value
	tree_node* tree_getIterator(tree* bt); // return iterator on root
        tree_node* tree_getSon(tree_node*, int); // return son i of the node, or NULL
        
// ALTERATIONS
        tree_node* tree_creatRoot(tree*,void*); // creat root with ancient root as rightSon

// MOVEMENTS
	void tree_postfix(tree_node*, void (*)(tree_fonctor), void*, void*); // API for tree_postfix_rec function
        	void tree_postfix_rec(tree_node*, void (*)(tree_fonctor), void*, void*, int*, int); // apply fonctor on each node of given tree, in postfix order
	void tree_prefix(tree_node*, void (*)(tree_fonctor), void*, void*); // API for tree_prefix_rec function
        	void tree_prefix_rec(tree_node*, void (*)(tree_fonctor), void*, void*, int*, int); // apply fonctor on each node of given tree, in prefix order
	//void tree_eachSon(tree_node*, void (*)(tree_node*, void*, int*, int), void*, int*);// apply fonctor on each direct son of the given node TODO


#endif
