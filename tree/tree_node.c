#include "tree.h"




/****************************************************************
 * INITIALIZING
 ****************************************************************/


/*
 * NODE INIT
 */
// initialize the node. Be careful : no free, just initialize.
int tree_node_init(tree_node* node, void* value, tree_node* father, tree* t) {
        // ATTRIBUTES
                node->father = father;
                node->value = value;
                node->tree = t;
                if(t != NULL) 
                        t->nodeCnt++;
        return 1;
}



/*
 * NODE MALLOC
 */
// creat, initialize and return a new node
tree_node* tree_node_malloc(void* value, tree_node* father, tree* t) {
        tree_node* node = malloc(sizeof(tree_node));
        if(node != NULL) {
                node->sons = list_malloc();
                tree_node_init(node, value, father, t);
        }
        return node;
}



/*
 * NODE FREE
 */
// free the node. not the node.
void* tree_node_free(tree_node* node) {
        void* value = node->value;
        if(node->tree != NULL)
                node->tree->nodeCnt--;
        list_free(node->sons);
        free(node); // free
        return value;
}







