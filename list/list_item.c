#include "list_item.h"




/****************************************************************
 * INITIALIZING
 ****************************************************************/


/*
 * INIT
 */
// initialize item's values
int list_item_init(list_item* it, list* l, void* value) {
        if(it != NULL) {
                it->list = l;
                it->value = value;
                it->next = NULL;
                it->prev = NULL;
        } else
                return 0;
        return 1;
}



/*
 * MALLOC
 */
// create, initialize and return a list_item
list_item* list_item_malloc(list* l, void* value) {
        list_item* it = malloc(sizeof(list_item));
        if(it != NULL)
                list_item_init(it, l, value);
        return it;
}



/*
 * FREE
 */
// free the item and return it's value
void* list_item_free(list_item* it) {
        void* value = NULL;
        if(it != NULL) {
                value = it->value;
                free(it);
        }
        return value;
}





/****************************************************************
 * PREDICATS
 ****************************************************************/





/****************************************************************
 * MOVEMENTS
 ****************************************************************/





/****************************************************************
 * ACCESSORS
 ****************************************************************/




/****************************************************************
 * ALTERATIONS
 ****************************************************************/
