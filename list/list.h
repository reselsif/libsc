#ifndef LIBSC_LIST_H_INCLUDED
#define LIBSC_LIST_H_INCLUDED

/*
 * LIBRARIES
 */
#include "list_item.h"



/*
 * EXPLANATIONS
 */
/*
List object is a generalist list. (contain only adress)
List never affect object pointing by adress. (except by user fonctors)
First object is list->first, and last one is list->last.
If (first == NULL), list is emtpy
If (first == last), list have one single element
for walk in list, use that type of code :
        list_iterator it = list->first;
        while(it != NULL) {
                // treatment
                it = it->next;
        }
        // ending
Lists can be readed in reverse mode, with :
        list_iterator it = list->last;
        while(it != NULL) {
                // treatment
                it = it->prev;
        }
        // ending

*/



/*
 * PREDECLARATIONS
 */
// list object
struct list_ {
        // ATTRIBUTES
	list_item* first; // first item, NULL if no item
	list_item* last; // last item, NULL if no item
	int itemCnt; // item counter
        // METHODS
	int (*isFirst)(list_iterator);
	int (*isLast)(list_iterator);
	int (*targetItem)(list_iterator);
        int (*isEmpty)(list*);

	void* (*getValue)(list_iterator);
	void  (*setValue)(list_iterator, void*);
        list_iterator (*getIterator)(list*);

        void (*addItem)(list*, void*);
        void (*addItemNext)(list_iterator, void*);
        void (*addItemPrev)(list_iterator, void*);
        void* (*delItem)(list_iterator);
};
// typedef find in list_item.h


// fonctor's arguments
typedef struct {
	list_item* item; // item
	void* argv; // argument send for fonctor
	void* ret; // value return 
	int* termine; // if true, moves in list is interupt
	int range; // range of item in list
} list_fonctor;





/*
 * PROTOTYPES
 */
// INITIALIZING (no struct methods)
        void list_init(list*); // initialize given list
        list* list_malloc(); // create and initialize a list
        void list_free(list*); // free given list, no item's values free, be careful !

// PREDICATS
        int list_isFirst(list_iterator); // return 1 if targeted item is the first of list
        int list_isLast(list_iterator); // return 1 if targeted item is the last of list
        int list_targetItem(list_iterator); // return 1 if iterator point to an item
        int list_isEmpty(list*); // return 1 if list have no item, 0 else

// MOVEMENTS

// ACCESSORS
        void* list_getValue(list_iterator); // return the item value
        void  list_setValue(list_iterator, void*); // modify item's value
        list_iterator list_getIterator(list*); // return an new iterator who target the given list
        list_iterator list_getItem(list*, int); // return an new iterator who target the element number i in given list (first <=> i = 0)

// ALTERATIONS
        void list_addItem(list*, void*); // add an item in list at end
        void list_addItemNext(list_iterator, void*); // add an item in list just after iterator, initialized with given value. if no item in list, no effect.
        void list_addItemPrev(list_iterator, void*); // add an item in list just after iterator, initialized with given value. if no item in list, no effect.
        void* list_delItem(list_iterator); // free item and go to previous one. Return item value

// MOVEMENTS
        void list_applyFonctor(list*, void (*)(list_fonctor), void*, void*); // apply the given fonctor to each element of list.
        void list_applyFonctorInverse(list*, void (*)(list_fonctor), void*, void*); // apply the given fonctor to each element of list, in inversed order.

#endif
