#ifndef LIBSC_LIST_ITEM_H_INCLUDED
#define LIBSC_LIST_ITEM_H_INCLUDED

/*
 * LIBRARIES
 */
#include <stdlib.h>




/*
 * PREDECLARATIONS
 */
// item of a list
typedef struct list_ list; // will be find in list.h
typedef struct list_item_ list_item;
struct list_item_ {
        // ATTRIBUTES
	void* value; // value of item
	list_item* next; // next item, NULL if not next item.
	list_item* prev; // previous item, NULL if not prev item.
        list* list; // list where item is
};


// list iterator: simple shortcut to item memory adress
typedef struct list_item_* list_iterator;




/*
 * PROTOTYPES
 */
// INITIALIZING (no struct methods)
        int list_item_init(list_item*, list*, void*);// initialize item's values
        list_item* list_item_malloc(list*, void*); // create, initialize and return a list_item
        void* list_item_free(list_item*); // free the item, (not targets !)

// PREDICATS

// MOVEMENTS

// ALTERATIONS




#endif
