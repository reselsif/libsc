#include "list.h"




/****************************************************************
 * INITIALIZING
 ****************************************************************/


/*
 * INIT
 */
// initialize given list
void list_init(list* l) {
        // ATTRIBUTES
	// No items
	l->first = NULL;
	l->last = NULL;
	l->itemCnt = 0;
        // METHODS
                l->isFirst = &list_isFirst;
                l->isLast = &list_isLast;
                l->targetItem = &list_targetItem;
                l->isEmpty = &list_isEmpty;

                l->getValue = &list_getValue;
                l->setValue = &list_setValue;
                l->getIterator = &list_getIterator;

                l->addItem = &list_addItem;
                l->addItemNext = &list_addItemNext;
                l->addItemPrev = &list_addItemPrev;
                l->delItem = &list_delItem;
}



/*
 * MALLOC
 */
// create and initialize a list
list* list_malloc() {
        list* l = malloc(sizeof(list));
        if(l != NULL) // if alloc success
	        list_init(l);
        return l;
}



/*
 * FREE
 */
// free given list and items. BE CAREFUL : values not free !
void list_free(list* l) {
        // for each item, call the free on it
        list_iterator it = l->first, jt;
        while(it != NULL) {
                jt = it;
                it = it->next;
                list_item_free(jt);
        }
        free(l);
}





/****************************************************************
 * PREDICATS
 ****************************************************************/


/*
 * IS FIRST
 */
// return 1 if targeted item is the first of list
int list_isFirst(list_iterator it) {
        return (it != NULL && it != NULL && it->list->first == it);
}



/*
 * IS LAST
 */
// return 1 if targeted item is the last of list
int list_isLast(list_iterator it) {
        return (it != NULL && it != NULL && it->list->last == it);
}



/*
 * TARGET ITEM
 */
// return 1 if iterator point to an item
int list_targetItem(list_iterator it) {
        return (it != NULL);
}



/*
 * IS EMPTY
 */
// return 1 if list have no item, 0 else
int list_isEmpty(list* l) {
	return (l != NULL && l->first == NULL);
}





/****************************************************************
 * MOVEMENTS
 ****************************************************************/





/****************************************************************
 * ACCESSORS
 ****************************************************************/


/*
 * GET VALUE
 */
// return the item value
void* list_getValue(list_iterator it) {
        return it->value;
}




/*
 * SET VALUE
 */
// return the item value
void list_setValue(list_iterator it, void* newValue) {
        it->value = newValue;
}



/*
 * GET ITERATOR
 */
// return a list_iterator, who target the given list, and the first item
list_iterator list_getIterator(list* l) {
        return l->first;
}



/*
 * GET ITEM
 */
// return an new iterator who target the element number i in given list (first <=> i = 0)
list_iterator list_getItem(list* l, int i) {
	list_iterator target = NULL;
	list_iterator it = l->first;
        for(; l->targetItem(it) && i > 0; it = it->next, i--) {}
	if(i == 0) target = it;
	return target;
}



/****************************************************************
 * ALTERATIONS
 ****************************************************************/


/*
 * ADD ITEM
 */
// add an item in list at end
void list_addItem(list* l, void* value) {
        // creat new item
        list_item* item = list_item_malloc(l, value);
        if(item != NULL) {
                // if no item in list, this item is the first, and the last
                if(l->last == NULL) {
                        l->first = item;
                        l->last = item;
                } else {
                        l->last->next = item;
                        item->prev = l->last;
                        l->last = item;
                }
                // one more item
                l->itemCnt++;
        }
}



/*
 * ADD ITEM NEXT
 */
// add an item in list just after iterator, initialized with given value.
// if no item in list, no effect.
void list_addItemNext(list_iterator it, void* value) {
        if(it == NULL) return; // protection
        // creat new item, between item A and item B
        list_item* item = list_item_malloc(it->list, value);
        if(item == NULL) return;
        // initialize values
        item->prev = it; // item->prev is A
        if(it != NULL) { // if A exist
                item->next = it->next; // next is B 
                if(item->next != NULL) // if B exist
                        item->next->prev = item; // previous of B is item
                it->next = item; // next of A is item
        }
        if(it->list != NULL) {
                // increment counter
                it->list->itemCnt++;
                // this item is it the last ?
                if(item->next == NULL)
                        it->list->last = item;
                if(it->list->itemCnt == 1) // if it's the first item
                        it->list->first = item;
        }
        it = item; // target the new item
}



/*
 * ADD ITEM PREV
 */
// add an item in list just after iterator, initialized with given value. 
// if no item in list, no effect.
void list_addItemPrev(list_iterator it, void* value) {
        if(it == NULL) return; // protection
        // creat new item, between item A and item B
        list_item* item = list_item_malloc(it->list, value);
        if(item == NULL) return;
        // initialize values
        item->next = it; // item->next is A
        if(it != NULL) { // if A exist
                item->prev = it->prev; // prev is B 
                if(item->prev != NULL) // if B exist
                        item->prev->next = item; // next of B is item
                it->prev = item; // prev of A is item
        }
        if(it->list != NULL) {
                // increment counter
                it->list->itemCnt++;
                // this item is it the first ?
                if(item->prev == NULL)
                        it->list->first = item;
                if(it->list->itemCnt == 1) // if it's the first item
                        it->list->last = item;
        }
        it = item; // target the new item
}



/*
 * DEL ITEM
 */
// delete item and go to previous one, or first of list of no previous. Return item value.
void* list_delItem(list_iterator it) {
        void* value = it->value;
        list_item* previous = it->prev;
        list_item* next = it->next;
        list* list = it->list;

        // delete item
        free(it);
        // connect previous and next items
        if(previous != NULL) {
                previous->next = next;
                it = previous;
        } else if(list != NULL) // if no previous, this item is the first
                list->first = next; // so the new first is the next !

        if(next != NULL) {
                next->prev = previous;
                it = next;
        } else if(list != NULL) // if no next, this item is the last
                list->last = previous; // so the new last is the prev !

        if(list != NULL)
                list->itemCnt--; // decrement counter
        return value;
}





/****************************************************************
 * MOVEMENTS
 ****************************************************************/
/*
 * LIST APPLY FONCTOR
 */
// apply the given fonctor to each element of list.
void list_applyFonctor(list* l, void (*fonctor)(list_fonctor), 
		void* arguments, void* returnValue) {
	// INITIALIZATIONS
	int termine = 0; // 1 if must end
	int range = 0; // place in list
        list_iterator it = l->first; // creat iterator on first item


        // APPLY FOR EACH ITEM
        while(l->targetItem(it) && !termine) {
		list_fonctor argv = {it, arguments, returnValue, &termine, range};
                fonctor(argv); // call to the fonctor
                it = it->next; // go next item
		range++; // next place
        }
}



/*
 * LIST APPLY FONCTOR INVERSE
 */
// apply the given fonctor to each element of list, in inversed order.
void list_applyFonctorInverse(list* l, void (*fonctor)(list_fonctor), 
		void* arguments, void* returnValue) {
	// INITIALIZATIONS
	int termine = 0; // 1 if must end
	int range = l->itemCnt-1; // place in list
        list_iterator it = l->last; // creat iterator on last item


        // APPLY FOR EACH ITEM
        while(l->targetItem(it) && !termine) {
		list_fonctor argv = {it, arguments, returnValue, &termine, range};
                fonctor(argv); // call to the fonctor
                it = it->prev; // go next item
		range--; // next place
        }
}



