#ifndef READOPT_H_INCLUDED
#define READOPT_H_INCLUDED



/*
 * LIBRARIES
 */
// LIBS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



/*
 * DEFINES
 */




/*
 * PREDECLARATIONS
 */
// Return copy of asked option, or empty string
char* readopt(int argc, char* argv[], char option);
// Return pointer to asked option, or NULL
char* readopt_hard(int argc, char* argv[], char option);





/*
 * PROTOTYPES
 */



#endif
