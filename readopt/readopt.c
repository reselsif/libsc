#include "readopt.h"






/*
 * READOPT
 */
// Return copy of asked option, or empty string
char* readopt(int argc, char* argv[], char option) {
        // take option value or NULL
        char* value = readopt_hard(argc, argv, option);
        char* target = NULL;
        // copy value
        if(value != NULL) {
                target = malloc(strlen(value)+1*sizeof(char));
                strcpy(target, value);
        } else {
                target = malloc(sizeof(char));
                target[0] = '\0';
        }
        // return
        return target;
}



/*
 * READOPT_HARD
 */
// Return pointer to asked option, or NULL
char* readopt_hard(int argc, char* argv[], char option) {
        char* target = NULL;
        // Creat option
        char option_complete[3] = "-x";
                option_complete[1] = option;
        // Search it
        int i = 0, p = -1;
        for(; i < argc && p != 0; i++) {
                if(strcmp(argv[i], option_complete) == 0) {
                        target = argv[i];
                        p = i+1;
                }
        }
        // read associate option
        // some protections. 
        if(target != NULL   &&   p < argc   &&   !(strlen(argv[p]) == 2   &&   argv[p][0] == '-') ) 
                target = argv[p]; 
        else    target = NULL;
        // return
        return target;
}




