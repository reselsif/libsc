#include "graph.h"


/****************************************************************
 * INITIALIZING
 ****************************************************************/


/*
 * INIT
 */
// initialize given graph
void graph_init(graph* g) {
        // ATTRIBUTES
	// No items
	g->first = NULL;
	g->last = NULL;
	g->itemCnt = 0;
        // METHODS
                g->isFirst = &graph_isFirst;
                g->isLast = &graph_isLast;
                g->targetItem = &graph_targetItem;
                g->isEmpty = &graph_isEmpty;

                g->getValue = &graph_getValue;
                g->setValue = &graph_setValue;
                g->getIterator = &graph_getIterator;

                g->addItem = &graph_addItem;
                g->addItemNext = &graph_addItemNext;
                g->addItemPrev = &graph_addItemPrev;
                g->delItem = &graph_delItem;
}



/*
 * MALLOC
 */
// create and initialize a graph
graph* graph_malloc() {
        graph* l = malloc(sizeof(graph));
        if(l != NULL) // if alloc success
	graph_init(l);
        return l;
}



/*
 * FREE
 */
// free given graph and items. BE CAREFUL : values not free !
void graph_free(graph* l) {
        // for each item, call the free on it
        // TODO
        free(l);
}





/****************************************************************
 * PREDICATS
 ****************************************************************/


/*
 * IS FIRST
 */
// return 1 if targeted item is the first of graph
int graph_isFirst(graph_iterator* it) {
        return (it != NULL && it != NULL && it->graph->first == it);
}



/*
 * IS LAST
 */
// return 1 if targeted item is the last of graph
int graph_isLast(graph_iterator* it) {
        return (it != NULL && it != NULL && it->graph->last == it);
}



/*
 * TARGET ITEM
 */
// return 1 if iterator point to an item
int graph_targetItem(graph_iterator* it) {
        return (it != NULL);
}



/*
 * IS EMPTY
 */
// return 1 if graph have no item, 0 else
int graph_isEmpty(graph* l) {
	return (l != NULL && l->first == NULL);
}





/****************************************************************
 * MOVEMENTS
 ****************************************************************/





/****************************************************************
 * ACCESSORS
 ****************************************************************/


/*
 * GET VALUE
 */
// return the item value
void* graph_getValue(graph_iterator* it) {
        return it->value;
}




/*
 * SET VALUE
 */
// return the item value
void graph_setValue(graph_iterator* it, void* newValue) {
        it->value = newValue;
}



/*
 * GET ITERATOR
 */
// return a graph_iterator, who target the given graph, and the first item
graph_iterator* graph_getIterator(graph* l) {
        return l->first;
}



/*
 * GET ITEM
 */
// return an new iterator who target the element number i in given graph (first <=> i = 0)
graph_iterator* graph_getItem(graph* l, int i) {
	graph_iterator* target = NULL;
	graph_iterator* it = l->first;
        for(; l->targetItem(it) && i > 0; it = it->next, i--) {}
	if(i == 0) target = it;
	return target;
}



/****************************************************************
 * ALTERATIONS
 ****************************************************************/


/*
 * ADD ITEM
 */
// add an item in graph at end
void graph_addItem(graph* l, void* value) {
        // creat new item
        graph_item* item = graph_item_malloc(l, value);
        if(item == NULL) return;
        // if no item in graph, this item is the first, and the last
        if(l->last == NULL) {
                l->first = item;
                l->last = item;
        } else {
                l->last->next = item;
                item->prev = l->last;
                l->last = item;
        }
        // one more item
        l->itemCnt++;
}



/*
 * ADD ITEM NEXT
 */
// add an item in graph just after iterator, initialized with given value.
// if no item in graph, no effect.
void graph_addItemNext(graph_iterator* it, void* value) {
        if(it == NULL) return; // protection
        // creat new item, between item A and item B
        graph_item* item = graph_item_malloc(it->graph, value);
        if(item == NULL) return;
        // initialize values
        item->prev = it; // item->prev is A
        if(it != NULL) { // if A exist
                item->next = it->next; // next is B 
                if(item->next != NULL) // if B exist
                        item->next->prev = item; // previous of B is item
                it->next = item; // next of A is item
        }
        if(it->graph != NULL) {
                // increment counter
                it->graph->itemCnt++;
                // this item is it the last ?
                if(item->next == NULL)
                        it->graph->last = item;
                if(it->graph->itemCnt == 1) // if it's the first item
                        it->graph->first = item;
        }
        it = item; // target the new item
}



/*
 * ADD ITEM PREV
 */
// add an item in graph just after iterator, initialized with given value. 
// if no item in graph, no effect.
void graph_addItemPrev(graph_iterator *it, void* value) {
        if(it == NULL) return; // protection
        // creat new item, between item A and item B
        graph_item* item = graph_item_malloc(it->graph, value);
        if(item == NULL) return;
        // initialize values
        item->next = it; // item->next is A
        if(it != NULL) { // if A exist
                item->prev = it->prev; // prev is B 
                if(item->prev != NULL) // if B exist
                        item->prev->next = item; // next of B is item
                it->prev = item; // prev of A is item
        }
        if(it->graph != NULL) {
                // increment counter
                it->graph->itemCnt++;
                // this item is it the first ?
                if(item->prev == NULL)
                        it->graph->first = item;
                if(it->graph->itemCnt == 1) // if it's the first item
                        it->graph->last = item;
        }
        it = item; // target the new item
}



/*
 * DEL ITEM
 */
// delete item and go to previous one, or first of graph of no previous. Return item value.
void* graph_delItem(graph_iterator *it) {
        void* value = it->value;
        graph_item* previous = it->prev;
        graph_item* next = it->next;
        graph* graph = it->graph;

        // delete item
        free(it);
        // connect previous and next items
        if(previous != NULL) {
                previous->next = next;
                it = previous;
        } else if(graph != NULL) // if no previous, this item is the first
                graph->first = next; // so the new first is the next !

        if(next != NULL) {
                next->prev = previous;
                it = next;
        } else if(graph != NULL) // if no next, this item is the last
                graph->last = previous; // so the new last is the prev !

        if(graph != NULL)
                graph->itemCnt--; // decrement counter
        return value;
}





/****************************************************************
 * MOVEMENTS
 ****************************************************************/
/*
 * LIST APPLY FONCTOR
 */
// apply the given fonctor to each element of graph.
void graph_applyFonctor(graph* l, void (*fonctor)(graph_fonctor), 
		void* arguments, void* returnValue) {
	// INITIALIZATIONS
	int termine = 0; // 1 if must end
	int range = 0; // place in graph
        graph_iterator* it = l->first; // creat iterator on first item


        // APPLY FOR EACH ITEM
        while(l->targetItem(it) && !termine) {
		graph_fonctor argv = {it, arguments, returnValue, &termine, range};
                fonctor(argv); // call to the fonctor
                it = it->next; // go next item
		range++; // next place
        }
}



/*
 * LIST APPLY FONCTOR INVERSE
 */
// apply the given fonctor to each element of graph, in inversed order.
void graph_applyFonctorInverse(graph* l, void (*fonctor)(graph_fonctor), 
		void* arguments, void* returnValue) {
	// INITIALIZATIONS
	int termine = 0; // 1 if must end
	int range = l->itemCnt-1; // place in graph
        graph_iterator* it = l->last; // creat iterator on last item


        // APPLY FOR EACH ITEM
        while(l->targetItem(it) && !termine) {
		graph_fonctor argv = {it, arguments, returnValue, &termine, range};
                fonctor(argv); // call to the fonctor
                it = it->prev; // go next item
		range--; // next place
        }
}





