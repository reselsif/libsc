#ifndef LIBSC_GRAPH_ITEM_H_INCLUDED
#define LIBSC_GRAPH_ITEM_H_INCLUDED

/*
 * LIBRARIES
 */
#include <stdlib.h>


/*
 * DEFINES
 */



/*
 * PREDECLARATIONS
 */
// item of a graph
typedef struct graph_ graph; // will be find in graph.h
typedef struct graph_item_ graph_item;
struct graph_item_ {
        // ATTRIBUTES
	void* value; // value of item
        //graph_item* arc/arrête;
        graph* graph; // graph where item is
};


// graph iterator: simple shortcut to item memory adress
typedef struct graph_item_* graph_iterator;



/*
 * PROTOTYPES
 */





#endif
