#ifndef LIBSC_GRAPH_H_INCLUDED
#define LIBSC_GRAPH_H_INCLUDED

/*
 * LIBRARIES
 */
#include "graph_item.h"


/*
 * DEFINES
 */



/*
 * PREDECLARATIONS
 */
// list object
struct graph_ {
        // ATTRIBUTES
	graph_item* first; // first item, NULL if no item
	graph_item* last; // last item, NULL if no item
	int itemCnt; // item counter
        // METHODS
	int (*isFirst)(graph_iterator*);
	int (*isLast)(graph_iterator*);
	int (*targetItem)(graph_iterator*);
        int (*isEmpty)(graph*);

	void* (*getValue)(graph_iterator*);
	void  (*setValue)(graph_iterator*, void*);
        graph_iterator* (*getIterator)(graph*);

        void (*addItem)(graph*, void*);
        void (*addItemNext)(graph_iterator*, void*);
        void (*addItemPrev)(graph_iterator*, void*);
        void* (*delItem)(graph_iterator*);
};
// typedef find in graph_item.h


// fonctor's arguments
typedef struct {
	graph_item* item; // item
	void* argv; // argument send for fonctor
	void* ret; // value return 
	int* termine; // if true, moves in graph is interupt
	int range; // range of item in graph
} graph_fonctor;



/*
 * PROTOTYPES
 */





#endif
