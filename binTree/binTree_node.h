#ifndef LIBSC_BINTREE_NODE_H_INCLUDED
#define LIBSC_BINTREE_NODE_H_INCLUDED



/*
 * LIBRARIES
 */
#include <stdlib.h>





/*
 * PREDECLARATIONS
 */
// node of binarie tree, use void* values
typedef struct binTree binTree;
struct binTree_node {
        // ATTRIBUTES
                void* value;
                struct binTree_node* rightSon; // right son node
                struct binTree_node* leftSon; // left son node
                struct binTree_node* father; // father node
                binTree* tree; // tree where node is
};
typedef struct binTree_node binTree_node;
typedef struct binTree_node binTree_iterator; // simple shortcut for librarie user






/*
 * PROTOTYPES
 */
// INITIALIZING
        int binTree_node_init(binTree_node*, void*, binTree_node*, binTree*);// initialize the given node
        binTree_node* binTree_node_malloc(void*, binTree_node*, binTree*); // creat, initialize and return a new node
        void* binTree_node_free(binTree_node*); // free the node and return his value





#endif
