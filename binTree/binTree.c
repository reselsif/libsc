#include "binTree.h"




/****************************************************************
 * INITIALIZING
 ****************************************************************/


/*
 * INIT
 */
// initialize the binarie tree
int binTree_init(binTree* bt, void* rootValue) {
        if(bt != NULL) {
                // ATTRIBUTES
                        bt->root = binTree_node_malloc(rootValue, NULL, bt);
                        bt->nodeCnt = 1; 
                        return 1;
        }
        return 0;
}



/*
 * BINARIE TREE MALLOC
 */
// Creat, initialize and return a binarie tree
binTree* binTree_malloc(void* value) {
        binTree* bt = malloc(sizeof(binTree));
        binTree_init(bt, value);
        return bt;
}



/*
 * FREE
 */
// call a free on all the nodes, and desinitialize the tree.
// CAREFUL: values are not free !
void binTree_free(binTree* bt) {
        // free on all the nodes with postfix movement
        binTree_postfix(bt->root, &binTree_free_fonctor, NULL, NULL);
        // free on tree
        free(bt);
}


/*
 * FREE FONCTOR
 */
// Use by binTree_free for free all the nodes
void binTree_free_fonctor(binTree_fonctor argv) {
        binTree_node_free(argv.node);
}


/****************************************************************
 * PREDICATS
 ****************************************************************/


/*
 * HAVE RIGHT SON
 */
// return 1 if node have a right son, 0 if not
int binTree_haveRightSon(binTree_node* it) {
          return (it != NULL && it->rightSon != NULL);
}




/*
 * HAVE LEFT SON
 */
// return 1 if node have a left son, 0 if not
int binTree_haveLeftSon(binTree_node* it) {
          return (it != NULL && it->leftSon != NULL);
}




/*
 * HAVE FATHER
 */
// return 1 if node have a father, 0 if not
int binTree_haveFather(binTree_node* it) {
          return (it != NULL && it->father != NULL);
}



/*
 * TARGET NODE
 */
// return 1 if iterator targets a node
int binTree_targetNode(binTree_node* it) {
        return (it != NULL);
}





/****************************************************************
 * ACCESSORS
 ****************************************************************/


/*
 * GET VALUE
 */
// return target node value
void* binTree_getValue(binTree_node* it) {
        if(it != NULL)
                return it->value;
        return NULL;
}



/*
 * SET VALUE
 */
// modify target node value and return the ancient value
void* binTree_setValue(binTree_node* it, void* value) {
        void* ancientValue = NULL;
        if(it != NULL) {
                ancientValue = it->value;
                it->value = value;
        }
        return ancientValue;
}



/*
 * GET ITERATOR
 */
// return iterator on root
binTree_node* binTree_getIterator(binTree* bt) {
        return bt->root;
}





/****************************************************************
 * ALTERATIONS
 ****************************************************************/


/*
 * CREAT ROOT
 */
// creat root if no root in given binTree
binTree_node* binTree_creatRoot(binTree* bt, void* value) {
        if(bt->root != NULL) return bt->root;
        binTree_node* it = binTree_node_malloc(value, NULL, bt); // create node
        // initialize it
        if(it != NULL) {
                bt->root = it;
        }
        return it;
}



/*
 * ADD RIGHT SON
 */
// if possible, add an rightSon to iterator node
binTree_node* binTree_addRightSon(binTree_node* it, void* value) {
        if(it == NULL) // tree is emtpy, so, nothing to do
                return NULL;
        else if( ! binTree_haveRightSon(it)) 
                it->rightSon = binTree_node_malloc(value, it, it->tree); // create node
        return it->rightSon;
}




/*
 * ADD LEFT SON
 */
// if possible, add an LSon to iterator node
binTree_node* binTree_addLeftSon(binTree_node* it, void* value) {
        if(it == NULL) // tree is emtpy, so, nothing to do
                return NULL;
        else if( ! binTree_haveLeftSon(it))
                        it->leftSon = binTree_node_malloc(value, it, it->tree); // create node
        return it->leftSon;
}






/****************************************************************
 * MOVEMENTS
 ****************************************************************/
// 3 patterns of movements : prefix, postfix and infix
// fonctor is a void function, use void arguments (given in moves function at 3 arguments),
// and a return value (int type)
//      void (*fonctor)(binTree_node*, void*, int*, int)


/*
 * POSTFIX
 */
// API for binTree_postfix_rec function
void binTree_postfix(binTree_node* begin, void (*fonctor)(binTree_fonctor),
                void* argv, void* returnValue) {
	int termine = 0;
        binTree_postfix_rec(begin, fonctor, argv, returnValue, &termine, 0);
}

/*
 * POSTFIX REC
 */
// apply fonctor on each node of given binTree, in postfix order
void binTree_postfix_rec(binTree_node* it, void (*fonctor)(binTree_fonctor), 
                void* argv, void* returnValue, int* termine, int depth) {
	if(!(*termine)) {
        	if(binTree_haveLeftSon(it)) {
                	binTree_postfix_rec(it->leftSon, fonctor, argv, 
					returnValue, termine, depth+1);
        	}
	}
	if(!(*termine)) {
        	if(binTree_haveRightSon(it)) {
                	binTree_postfix_rec(it->rightSon, fonctor, argv, 
					returnValue, termine, depth+1);
        	}
	}
	if(!(*termine)) {
		binTree_fonctor f = {it, argv, returnValue, termine, depth};
        	fonctor(f);
	}
}



/*
 * PREFIX
 */
// API for binTree_prefix_rec function
void binTree_prefix(binTree_node* begin, void (*fonctor)(binTree_fonctor),
                void* argv, void* returnValue) {
	int termine = 0;
        binTree_prefix_rec(begin, fonctor, argv, returnValue, &termine, 0);
}

/*
 * PREFIX REC
 */
// apply fonctor on each node of given binTree, in prefix order
void binTree_prefix_rec(binTree_node* it, void (*fonctor)(binTree_fonctor), 
                void* argv, void* returnValue, int* termine, int depth) {
	if(!(*termine)) {
		binTree_fonctor f = {it, argv, returnValue, termine, depth};
        	fonctor(f);
	}
	if(!(*termine)) {
        	if(binTree_haveLeftSon(it)) {
                	binTree_prefix_rec(it->leftSon, fonctor, argv, 
					returnValue, termine, depth+1);
        	}
	}
	if(!(*termine)) {
        	if(binTree_haveRightSon(it)) {
                	binTree_prefix_rec(it->rightSon, fonctor, argv, 
					returnValue, termine, depth+1);
        	}
	}
}




/*
 * INFIX
 */
// API for binTree_infix_rec function
void binTree_infix(binTree_node* begin, void (*fonctor)(binTree_fonctor),
                void* argv, void* returnValue) {
	int termine = 0;
        binTree_infix_rec(begin, fonctor, argv, returnValue, &termine, 0);
}

/*
 * INFIX
 */
// apply fonctor on each node of given binTree, in infix order
void binTree_infix_rec(binTree_node* it, void (*fonctor)(binTree_fonctor), 
                void* argv, void* returnValue, int* termine, int depth) {
	if(!(*termine)) {
        	if(binTree_haveLeftSon(it)) {
                	binTree_infix_rec(it->leftSon, fonctor, argv, 
					returnValue, termine, depth+1);
        	}
	}
	if(!(*termine)) {
		binTree_fonctor f = {it, argv, returnValue, termine, depth};
        	fonctor(f);
	}
	if(!(*termine)) {
        	if(binTree_haveRightSon(it)) {
                	binTree_infix_rec(it->rightSon, fonctor, argv, 
					returnValue, termine, depth+1);
        	}
	}
}



