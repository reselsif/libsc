#ifndef LIBSC_BINTREE_INCLUDED
#define LIBSC_BINTREE_INCLUDED

/*
 * LIBRARIES
 */
#include "binTree_node.h"



/*
 * STRUCTURES
 */
// binarie tree, modes is binTree type
struct binTree {
        // ATTRIBUTS
        binTree_node *root; // root of binarie tree
        int nodeCnt; // number of nodes
};

// fonctor's arguments
typedef struct {
	binTree_node* node; // node
	void* argv; // argument send for fonctor
	void* ret; // value return 
	int* termine; // if true, moves in tree is interupt
	int depth; // depth of node
} binTree_fonctor;




/*
 * PROTOTYPES
 */
// INITIALIZING
        int binTree_init(binTree*, void*); // initialize the binarie tree
        binTree* binTree_malloc(void*); // Creat, initialize and return a binarie tree
        void binTree_free(binTree*); // call a free on all the nodes, and desinitialize the tree.
        void binTree_free_fonctor(binTree_fonctor); // Use by binTree_free for free all the nodes

// PREDICATS
        int binTree_haveRightSon(binTree_node*); // return 1 if node have a right son, 0 if not
        int binTree_haveLeftSon(binTree_node*); // return 1 if node have a left son, 0 if not
        int binTree_haveFather(binTree_node*); // return 1 if node have a father, 0 if not
        int binTree_targetNode(binTree_node* it); // return 1 if iterator targets a node

// ACCESSORS
        void* binTree_getValue(binTree_node*); // return target node value
        void* binTree_setValue(binTree_node*, void*); // modify target node value and return the ancient value
        
// ALTERATIONS
        binTree_node* binTree_creatRoot(binTree*,void*); // creat root with ancient root as rightSon
        binTree_node* binTree_addRightSon(binTree_node*, void*); // if possible, add an rightSon to iterator target
        binTree_node* binTree_addLeftSon(binTree_node*, void*); // if possible, add an LSon to iterator target

// MOVEMENTS
	void binTree_postfix(binTree_node*, void (*)(binTree_fonctor), void*, void*); // API for binTree_postfix_rec function
        	void binTree_postfix_rec(binTree_node*, void (*)(binTree_fonctor), void*, void*, int*, int); // apply fonctor on each node of given binTree, in postfix order
	void binTree_prefix(binTree_node*, void (*)(binTree_fonctor), void*, void*); // API for binTree_prefix_rec function
        	void binTree_prefix_rec(binTree_node*, void (*)(binTree_fonctor), void*, void*, int*, int); // apply fonctor on each node of given binTree, in prefix order
        	void binTree_infix_rec(binTree_node*, void (*)(binTree_fonctor), void*, void*, int*, int); // apply fonctor on each node of given binTree, in infix order

	
#endif
