#include "binTree.h"




/****************************************************************
 * INITIALIZING
 ****************************************************************/


/*
 * NODE INIT
 */
// initialize the node. Be careful : no free, just initialize.
int binTree_node_init(binTree_node* node, void* value, binTree_node* father, binTree* bt) {
        // ATTRIBUTES
                node->rightSon = node->leftSon = NULL;
                node->father = father;
                node->value = value;
                node->tree = bt;
                if(bt != NULL) 
                        bt->nodeCnt++;
        return 1;
}



/*
 * NODE MALLOC
 */
// creat, initialize and return a new node
binTree_node* binTree_node_malloc(void* value, binTree_node* father, binTree* bt) {
        binTree_node* node = malloc(sizeof(binTree_node));
        if(node != NULL) 
                binTree_node_init(node, value, father, bt);
        return node;
}



/*
 * NODE FREE
 */
// free the node and return his value
void* binTree_node_free(binTree_node* node) {
        void* value = node->value;
	// tree lost his node
        if(node->tree != NULL)
                node->tree->nodeCnt--;
	// father lost his son
	if(node->father != NULL) {
		if(node->father->leftSon == node)
			node->father->leftSon = NULL;
		else
			node->father->rightSon = NULL;
	}
        free(node); // free
        return value;
}







