/*
 * INCLUDES
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
// includes libraries
#include "../list/list.h"
#include "../stack/stack.h"
#include "../binTree/binTree.h"
#include "../tree/tree.h"
#include "../graph/graph.h"
#include "../readopt/readopt.h"
#include "../utils/utils.h"
#include "../utils/time.h"
#include "../utils/term.h"


/*
 * PROTOTYPES
 */
void example_list();
	void myListFonctor(list_fonctor);
void example_stack();
void example_binTree();
	void myBinTreeFonctor_display(binTree_fonctor);
	void myBinTreeFonctor_free(binTree_fonctor);
void example_tree();
	void myTreeFonctor_display(tree_fonctor);
	void myTreeFonctor_free(tree_fonctor);
void example_readopt(int, char**);
void example_utils();


/*
 * PREDECLARATIONS
 */
// testing structure
typedef struct {
        int c1;
        float c2;
} myOne;







/***********************************************
 * MAIN
 ***********************************************/
int main(int argc, char* argv[]) {
	srand(time(NULL)); // initialize random
        int choice = 0;
        printf("\t== EXAMPLES ==\n\t1. list\t 2.stack\n\t3. binTree\n\t4. tree\t5. readopt\n\t6. utils\n");
        while(choice < 1 || choice > 6) scanf("%i", &choice);
        switch(choice) {
                case 1: // view list for the most describe example
                        example_list(); break;
                case 2: // all the other module follow the same implementation
                        example_stack(); break;
                case 3:
                        example_binTree(); break;
                case 4:
                        example_tree(); break;
                case 5:
                        example_readopt(argc, argv); break;
                case 6:
                        example_utils(); 
        }
        return EXIT_SUCCESS;
}







/***********************************************
 * EXAMPLE BINTREE
 ***********************************************/
void example_binTree() {
        // INITIALIZATION
        // binTree
                int *value = malloc(sizeof(int));
               	*value = 42; // value of binTree's root
                binTree *myTree = binTree_malloc(value);
                printf("My binary tree have %i nodes\n", myTree->nodeCnt);
        // binTree_iterator
                binTree_iterator *myIt = myTree->root; // or binTree_getIterator(myTree);

        // CREATE 20 NODES
                int counter = 10;
                while(counter >= 0) {
                        int* value = malloc(sizeof(int));
                       	*value = randN(100);
                        if(!binTree_haveRightSon(myIt))
                                binTree_addRightSon(myIt, value);
                        int* value2 = malloc(sizeof(int));
                       	*value2 = randN(100);
                        if(!binTree_haveLeftSon(myIt))
                                binTree_addLeftSon(myIt, value2);
                        if(randN(2) == 0)
                                myIt = myIt->rightSon;
                        else
                                myIt = myIt->leftSon;
                        if(value != NULL)
                                printf("Creat node with value %i\n", *value);
                        if(value2 != NULL)
                                printf("Creat node with value %i\n", *value2);
                        counter--;
                }
                printf("My binary tree have currently %i nodes\n", myTree->nodeCnt);


        // DISPLAY ARBORESCENT
                // use prefix move with display fonctor
                binTree_prefix(myTree->root, &myBinTreeFonctor_display, NULL, NULL);


        // LIBERATIONS
                // use postfix move with free fonctor
                binTree_postfix(myTree->root, &myBinTreeFonctor_free, NULL, NULL);
		// free the tree, and all the nodes
        	binTree_free(myTree);
}


/*
 * MY BINTREE FONCTOR: DISPLAY
 */
// display the current node
void myBinTreeFonctor_display(binTree_fonctor argv) {
        int i = 0;
        for(i = 0; i < argv.depth; i++)
                printf("  ");
        if(i != 0) printf("|>");
        printf("%i\n", *((int*) (argv.node->value)));
}



/*
 * MY BINTREE FONCTOR: FREE
 */
// display the current node
void myBinTreeFonctor_free(binTree_fonctor argv) {
	int* nodeValue = (int*) (argv.node->value);
	printf("Node's with value %i was free\n", *nodeValue);
	free(nodeValue);
}







/***********************************************
 * EXAMPLE TREE
 ***********************************************/
void example_tree() {
        // INITIALIZATION
        // tree
                int *value = malloc(sizeof(int));
               	*value = 42; // value of tree's root
                tree *myTree = tree_malloc(value);
                printf("My tree have %i nodes\n", myTree->nodeCnt);
        // tree_iterator
                tree_iterator *myIt = myTree->root; // or tree_getIterator(myTree);

        // CREATE 6 LEVELS, with randN(5)+1 sons for each node
                int counter = 6, i = 0;
                while(counter >= 0) {
			*value = randN(100); // value for the node
			// creat the sons
			for(i = randN(5)+1; i >= 0; i--) {
				value = malloc(sizeof(int));
				*value = randN(100); // value for the node
				tree_addSon(myIt, value);
                        	if(value != NULL)
                                	printf("Creat node with value %i\n", *value);
			}
			// go on the first son
			myIt = myIt->sons->first->value;
                        counter--;
                }
                printf("My tree have currently %i nodes\n", myTree->nodeCnt);


        // DISPLAY ARBORESCENT
                // use prefix move with display fonctor
                tree_prefix(myTree->root, &myTreeFonctor_display, NULL, NULL);


        // LIBERATIONS
                // use postfix move with free fonctor
                tree_postfix(myTree->root, &myTreeFonctor_free, NULL, NULL);
		// free the tree, and all the nodes
        	tree_free(myTree);
}



/*
 * MY TREE FONCTOR: DISPLAY
 */
// display each node
void myTreeFonctor_display(tree_fonctor argv) {
        int i = 0;
        for(i = 0; i < argv.depth; i++)
                printf(" '");
        if(i != 0) printf("|>");
        printf("%i\n", *((int*) (argv.node->value)));
}



/*
 * MY TREE FONCTOR: FREE
 */
// display the current node
void myTreeFonctor_free(tree_fonctor argv) {
	int* nodeValue = (int*) (argv.node->value);
	printf("Node with value %i was free\n", *nodeValue);
	free(nodeValue);
}



/***********************************************
 * EXAMPLE LIST
 ***********************************************/
void example_list() {
        // CREAT MY LIST
        list *myList = list_malloc();
        // my list have...
        printf("my list have %i item\n", myList->itemCnt);

        // ADD SOME ITEM
                int i;
                for(i = 0; i < 10; i++) {
                        int* value = malloc(sizeof(int));
                        *value = randN(i+1);
                        myList->addItem(myList, value); 
                }
                // my list have...
                printf("my list have %i item\n", myList->itemCnt);


        // CREAT AN LIST ITERATOR, there is 2 different way, exactly the same
                // first one
                        list_iterator myIt1 = myList->getIterator(myList);
                // second one
                        list_iterator myIt2 = myList->first;


        // PRINT EACH ITEM OF MY LIST
                printf("Print each item : (");
                term_color(stdout, GREEN, BLACK);
                for(myIt1 = myList->first; myList->targetItem(myIt1); myIt1 = myIt1->next) {
                        int* value = myIt1->value;
                        printf("%i,", *value);
                }
                term_reset(stdout);
                printf(")\nReverse printing : (");
                term_color(stdout, GREEN, BLACK);
                for(myIt1 = myList->last; myList->targetItem(myIt1); myIt1 = myIt1->prev) {
                        int* value = myIt1->value;
                        printf("%i,", *value);
                }
                term_reset(stdout);
                printf(")\nNow, apply a fonctor to my list. We use it for add 10 to each item, and add the next node's value.\n");

                int arguments = 10;
                list_applyFonctor(myList, &myListFonctor, &arguments, NULL);


        // RE-DISPLAY FOR VERIFICATION
                for(myIt1 = myList->first; myList->targetItem(myIt1); myIt1 = myIt1->next) {
                        int* value = (myList->getValue(myIt1));
                        printf("item value : %i\n", *value);
                }
                printf("\n");


        // FREE LIST
                myIt1 = myList->first;
                while(myList->targetItem(myIt1)) {
                        int* value = myList->delItem(myIt1);
                        printf("item with value %i is free\n", *value);
                        free(value);
                        myIt1 = myList->first;
                }
        list_free(myList);
}






/*
 * MY LIST FONCTOR
 */
// the value are modified : it add argument's value to it
void myListFonctor(list_fonctor argv) {
        int* arg = argv.argv; // we know we use int arguments
        int* val = argv.item->value; // and node's values are int
        int* val_next = NULL; // value of next node
        if(argv.item->next != NULL) { // if next node exist
                val_next = argv.item->next->value; // use it
        }
       *val += *arg;
        if(val_next != NULL) // last node have no value to
               *val += *val_next; 
}








/***********************************************
 * EXAMPLE STACK
 ***********************************************/
void example_stack() {
        // TODO
}







/***********************************************
 * EXAMPLE READOPT
 ***********************************************/
void example_readopt(int argc, char* argv[]) {
        int j = 0;
        char i = 'a', *str = NULL;
        printf("ARGUMENTS :\n");
        for(; j < argc; j++) {
                printf("\toption %i : |%s|\n", j, argv[j]);
        }
        printf("READOPT :\n");
        for(; i < 'e'; i++) {
                str = readopt(argc, argv, i);
                printf("\toption %c : |%s|\n", i, str);
                free(str);
        }
}






/***********************************************
 * EXAMPLE UTILS
 ***********************************************/
void example_utils() {
        // RANDN
                printf("\nrandN(int): \n");
                int maxi = randN(100)+randN(100), i = 0;
                printf("\tThere is a number in [0;%i[ : %i\n", maxi, randN(maxi));
		for(; i < 5; i++)
                	printf("\tanother one : %i\n", randN(maxi));
        // SWAP
                printf("\nswap(void*, void*, long unsigned int): \n");
                // there are the variable we want to inverse
                int a = 42, b = 23;
                myOne c = {42, 3.14}, d ={23, 1.25};

                // initials displays
                printf("\ta = %i, b = %i\n", a, b);
                printf("\tc = %i,%f, d = %i,%f\n\tSWAP\n", c.c1, c.c2, d.c1, d.c2);

                // swap
                swap(&a, &b, sizeof(int));
                swap(&c, &d, sizeof(myOne));

                // after swap displays
                printf("\ta = %i, b = %i\n", a, b);
                printf("\tc = %i,%f, d = %i,%f\n\n", c.c1, c.c2, d.c1, d.c2);
}




