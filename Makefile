CC=gcc
FINALNAME=a.out




all: $(FINALNAME) clean


#			#
#	FINAL NAME	#
#			#
$(FINALNAME): example.o binTree.o list.o stack.o utils.o tree.o readopt.o
	gcc -o $(FINALNAME) *.o




#			#
#	MODULES		#
#			#
example.o: examples/ 
	gcc -c examples/*.c

utils.o: utils/
	gcc -c utils/*.c

binTree.o: binTree/
	gcc -c binTree/*.c

tree.o: tree/
	gcc -c tree/*.c

list.o: list/
	gcc -c list/*.c

stack.o: stack/
	gcc -c stack/*.c

readopt.o: readopt/
	gcc -c readopt/*.c




#			#
#	COMMANDS	#
#			#
clean:
	rm *.o

clear:
	rm *.o
	rm $(FINALNAME)



