#include "utils.h"






/*
 * RAND N
 */
// return random number between 0 included and N-1 included.
int randN(int N) {
	return (int) (rand() / (double)RAND_MAX * (N));
}





/*
 * SWAP
 */
// universal invertion of a and b. They must use the same memory size. Use the XOR swap 
//      algorithm
void swap(void* a, void* b, long unsigned int size) {
	// if a == b, invert is useless
	if(a != b) {
		// divided target into bytes
		unsigned char *A = (unsigned char*) a; 
		unsigned char *B = (unsigned char*) b;
		long unsigned int i = 0;
		// for each byte apply three XOR
		for(i = 0; i < size; i++) {
			A[i] ^= B[i];
			B[i] ^= A[i];
			A[i] ^= B[i];
		}
		// a and b are inverted !
	}
}



/*
 * RAND STR
 */
// Return a rand string contain given characters
char* randstr(size_t size, char* caracs) {
// INITIALIZATION
        unsigned int nb_caracs = strlen(caracs);
        unsigned int i = 0;
        char* ret = (char*)malloc(sizeof(char)*(size+1));
// TREATMENT
        for(; i < size; i++) {
                ret[i] = caracs[randN(nb_caracs)];
        }
        ret[i] = '\0';
        return ret;
}



