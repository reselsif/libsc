#ifndef LIBSC_TERM_H_INCLUDED
#define LIBSC_TERM_H_INCLUDED



/*
 * LIBRARIES
 */
#include <stdlib.h>
#include <stdio.h>




/*
 * DEFINES
 */




/*
 * PREDECLARATIONS
 */
// colors : 
//      0 black 
//      1 red
//      2 green
//      3 orange
//      4 blue
//      5 magenta
//      6 cyan
//      7 white
typedef enum { BLACK=0, RED=1, GREEN=2, ORANGE=3, BLUE=4, MAGENTA=5, CYAN=6, WHITE=7 } term_termcolor;





/*
 * PROTOTYPES
 */
void term_color(FILE*, const int, const int);
void term_reset(FILE*);
void term_clear(FILE*);



#endif
