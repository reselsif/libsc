#ifndef LIBSC_UTILS_H_INCLUDED
#define LIBSC_UTILS_H_INCLUDED



/*
 * LIBRARIES
 */
#include <stdlib.h>
#include <string.h>




/*
 * DEFINES
 */




/*
 * PREDECLARATIONS
 */





/*
 * PROTOTYPES
 */
	int randN(int); // return random number between 0 included and N-1 included.
	void swap(void*, void*, long unsigned int); // universal invertion of a and b. They must use the same memory size. Use the XOR swap algorithm
        char* randstr(size_t, char*); // Return a rand string contain given characters



#endif
