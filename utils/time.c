#include "time.h"




/******************************************************************
 * TIME METHODS
 ******************************************************************/
/*
 * GIVE TIME SINCE
 */
// return seconds passes since given clock_t
float timeSince(const clock_t t) {
	return ((float)(clock()-t) / CLOCKS_PER_SEC);
}



/*
 * WAITING FOR
 */
// stop the program for given milliseconds
void waitFor(const int milliseconds) {
#ifdef __GNUC__
	sleep((float)milliseconds / 1000.);
#elif __WIN32__
	sleep(milliseconds);
#endif
}




