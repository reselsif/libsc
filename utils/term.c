#include "term.h"




/******************************************************************
 * TERM METHODS
 ******************************************************************/
/*
 * TERM COLOR
 */
// modify color of text and background in terminal
void term_color(FILE* OUT, const int textc, const int backc) {
	fprintf(OUT, "\033[%i;%im", textc+30, backc+40);;
}


/*
 * TERM RESET
 */
// reset term
void term_reset(FILE* OUT) {
	fprintf(OUT, "\033[0m");
}


/*
 * TERM CLEAR
 */
// clear term
void term_clear(FILE* OUT) {
	fprintf(OUT, "\033[H\033[2J");
}




