#ifndef LIBSC_TIME_H_INCLUDED
#define LIBSC_TIME_H_INCLUDED



/*
 * LIBRARIES
 */
#include <stdlib.h>
#include <time.h>





/*
 * PROTOTYPES
 */
float timeSince(const clock_t);
void waitFor(const int);



#endif
